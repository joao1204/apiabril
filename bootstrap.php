<?php
require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// $paths = array("TExAPITest/Entities");
$paths = array(__DIR__."/src/TExAPITest/Entities");
$isDevMode = true;

$conn = array(
	"dbname" => "db_carro",
	"user" => "root",
	"password" => "123",
	"host" => "localhost",
	"driver" => "pdo_mysql"
	);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

$entityManager = EntityManager::create($conn, $config);

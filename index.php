<?php
$loader = require __DIR__ . '/vendor/autoload.php';

use TExAPITest\Repository\CarroRepository;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

$repository = new CarroRepository();

$app->get('/carros/{id}', function (Request $request, Response $response) use ($repository) {
    $result = $repository->buscarPorId($request->getAttribute('id'));

    if(is_null($result))
    	return $response->withJson(array('msg'=>'Registro inexistente'), 400);

    return $response->withJson($result);
    
});

$app->get('/carros/{limite}/{posicao}', function (Request $request, Response $response) use ($repository) {
	$limite = $request->getAttribute('limite');
	$posicao = $request->getAttribute('posicao');

    return $response->withJson($repository->buscarTodos($limite, $posicao));
});

$app->get('/carros/{params:.*}/{limite}/{posicao}', function (Request $request, Response $response) use ($repository) {
	$params = $request->getAttribute('params');
	$limite = $request->getAttribute('limite');
	$posicao = $request->getAttribute('posicao');

    return $response->withJson($repository->buscarPor($params, $limite, $posicao));
});

$app->post('/carros[/]', function (Request $request, Response $response) use ($repository) {
    $data = $request->getParsedBody();
	$result = $repository->novo($data);
    
	if(isset($result['success']) && !$result['success'])
    	return $response->withJson($result, 400);
    
    return $response->withJson($result);
});

$app->put('/carros/{id}', function (Request $request, Response $response) use ($repository) {
	$data = $request->getParsedBody();
    $result = $repository->alterar($data);
	
	if(isset($result['success']) && !$result['success'])
    	return $response->withJson($result, 400);
    
    return $response->withJson($result);
});

$app->delete('/carros/{id}', function (Request $request, Response $response) use ($repository) {

    $result = $repository->apagar($request->getAttribute('id'));

    if(!$result)
        return $response->withJson(array('success'=>false), 400);

	return $response->withJson(array('success' => true));
});

$app->run();

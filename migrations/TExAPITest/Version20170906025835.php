<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170906025835 extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $schema = new Schema();
        $this->addSql('SELECT 1');
        
        $table = $schema->createTable('carros');
        $table->addColumn('id', 'integer', array(
            'autoincrement' => true,
        ));
        $table->setPrimaryKey(array('id'));
        $table->addColumn('placa', 'string');
        $table->addColumn('modelo', 'string');
        $table->addColumn('rodas', 'integer');

        return $schema;

    }

    public function down(Schema $schema)
    {
         $schema->dropTable('carros');

    }
}

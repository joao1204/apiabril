<?php

namespace TExAPITest\Entities;
use TExAPITest\Entities\AutomovelEntityAbstract;

/**
 * @Entity
 * @Table(name="carros")
 */

class CarroEntity extends AutomovelEntityAbstract { 
	/**
     * @Column(type="string", name="modelo")
     */
	private $modelo;

	public function __construct($modelo, $placa, $rodas, $id = 0){
		$this->modelo = $modelo;
		$this->setPlaca($placa);
		$this->setRodas($rodas);
		$this->setId($id);
	}

	public function getModelo() : string {
		return $this->modelo;
	}

	public function setModelo($modelo){
		$this->modelo = $modelo;
	}
}
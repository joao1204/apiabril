<?php
namespace TExAPITest\Entities;

/**
 * @Entity
 * @Table(name="carros")
 */
/** @MappedSuperclass */
abstract class AutomovelEntityAbstract {
	/**
     * @Id
     * @GeneratedValue(strategy="AUTO")
     * @Column(type="integer", name="id")
     */
	private $id;
	/**
     * @Column(type="string", name="placa")
     */
	private $placa;
	/**
     * @Column(type="integer", name="rodas")
     */
	private $rodas;

	public function getId(): int {
		return $this->id;
	}

	public function setId(int $id){
		$this->id = $id;
	}

	public function getPlaca(): string {
		return $this->placa;
	}

	public function setPlaca(string $placa){
		$this->placa = $placa;
	}

	public function getRodas(): int {
		return $this->rodas;
	}

	public function setRodas(int $rodas){
		$this->rodas = $rodas;
	}
}


<?php

namespace TExAPITest\Persistence;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Connection {

	public $entityManager;
	private $entityPath;

	public function __construct($entityPath) {
			$this->entityPath = $entityPath;
			$this->entityManager = $this->createEntityManager();
	}

	public function createEntityManager() {
		$path = array("TExAPITest/Entities");
		$connectionOptions =  array (
		    "dbname" => "db_carro",
			"user" => "root",
			"password" => "123",
			"host" => "localhost",
			"driver" => "pdo_mysql"
		);

		$config = Setup::createAnnotationMetadataConfiguration($path, true);
		return EntityManager::create($connectionOptions, $config);
	}

	public function insert($carro){
		$this->entityManager->persist($carro);
		$this->entityManager->flush();
		return $carro;
	}
	public function update($carro){
		$this->entityManager->merge($carro);
		$this->entityManager->flush();
		return $carro;
	}
	public function delete($id){
		$this->entityManager->remove($this->entityManager->getPartialReference($this->entityPath, array('id' => $id)));
		$this->entityManager->flush();
	}
	public function findById($id){
		
		return $this->entityManager->find($this->entityPath, $id);
	}
	public function findAll($limite, $posicao){
		return $this->entityManager->getRepository($this->entityPath)->findBy(array(), array(), $limite,$posicao);
	}
	public function findBy($params, $limite, $posicao){
		return $this->entityManager->getRepository($this->entityPath)
								->findBy(
									array('rodas' => $params, 'placa' => $params, 'modelo' => $params), 
									$limite,$posicao);
	}
}

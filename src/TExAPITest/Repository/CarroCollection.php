<?php
namespace TExAPITest\Repository;
use TExAPITest\CarroEntity;

class CarroCollection{

	private $colecao;

    public function __construct($colecao = array()){
        $this->colecao = $colecao;
    }

	public function add($entidade) {

    }

    public function remove($entidade) {

    }

    public function current(): CarroEntity {
       return current($this->colecao);
    }

    public function key(): int {
       return key($this->colecao);
    }

    public function next(): void{
       
    }

    public function rewind(): void {
       reset($this->var);
    }

    public function valid(): bool {
        $key = key($this->colecao);
        return ($key !== NULL && $key !== FALSE);
    }

    public function count(): int {
        return count($this->colecao);
    }

}

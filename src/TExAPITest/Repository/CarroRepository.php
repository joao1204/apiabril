<?php
namespace TExAPITest\Repository;

use TExAPITest\Entities\CarroEntity;
use TExAPITest\Persistence\Connection;
use TExAPITest\Repository\CarroCollection;

class CarroRepository {

	private $entityManager;

    public function __construct(){
        $this->entityManager = new Connection('TExAPITest\Entities\CarroEntity');
    }

	public function buscarPorId(int $id) {
		$result = $this->entityManager->findById($id);
		if(is_null($result))
	    	return null;

		return $this->getArray($result);
	}

	public function buscarTodos($limite = null, $posicao = null) {
		$result = $this->entityManager->findAll($limite, $posicao);
		$return = array('total' => 0, 'limite' => (int)$limite, 'posicao' => (int)$posicao, 'items' => array());
		$colecao = new CarroCollection($result);

		foreach($result as $obj){
			$tmp = array();
			$tmp['id'] = $obj->getId();
			$tmp['modelo'] = $obj->getModelo();
			$tmp['placa'] = $obj->getPlaca();
			$tmp['rodas'] = $obj->getRodas();

			$return['items'][] = $tmp;
		}
		$return['total'] = $colecao->count();
		return $return;
	}

	public function buscarPor($params = [], $limite = null, $posicao = null) {
		$result = $this->entityManager->findBy($params, [], $limite, $posicao);
		$return = array('total' => 0, 'limite' => (int)$limite, 'posicao' => (int)$posicao, 'items' => array());
		$colecao = new CarroCollection($result);

		foreach($result as $obj){
			$tmp = array();
			$tmp['id'] = $obj->getId();
			$tmp['modelo'] = $obj->getModelo();
			$tmp['placa'] = $obj->getPlaca();
			$tmp['rodas'] = $obj->getRodas();

			$return['items'][] = $tmp;
		}
		$return['total'] = $colecao->count();
		return $return;
	}

	public function novo($entidade) {
		$return = array('success' => false, 'msg' => '');
		
		if(!empty($entidade['modelo']) && !empty($entidade['rodas']) && !empty($entidade['placa'])){
            if(preg_match('/^[A-Z]{3}-[0-9]{4}$/i', $entidade['placa'])){
        		$carro = new CarroEntity($entidade['modelo'], $entidade['placa'], $entidade['rodas']);
        		$return = $this->getArray($this->entityManager->insert($carro));
            } else {
                $return['msg'] = 'Placa no formato incorreto. Por favor utilize o formato: AAA-1234';
            }
        } else {
            $return['msg'] = 'Todos campos são obrigatórios';
        }

		return $return;
	}

	public function alterar($entidade) {

		$return = array('success' => false, 'msg' => '');
		if(!empty($entidade['modelo']) && !empty($entidade['rodas']) && !empty($entidade['placa'])){
            if(preg_match('/^[A-Z]{3}-[0-9]{4}$/i', $entidade['placa'])){
                $carro = new CarroEntity($entidade['modelo'], $entidade['placa'], $entidade['rodas'], $entidade['id']);
				$return = $this->getArray($this->repository->update($carro));
            } else {
                $return['msg'] = 'Placa no formato incorreto. Por favor utilize o formato: AAA-1234';
            }
        } else {
            $return['msg'] = 'Todos campos são obrigatórios';
        }

		return $return;
	}

	public function apagar(int $id) : bool{
		$result = $this->buscarPorId($id);

		if(is_null($result))
        	return false;

		$this->entityManager->delete($id);
		return true;
	}

	public function getArray($entity){
		return array(
			'id' =>  $entity->getId(),
			'modelo' =>  $entity->getModelo(),
			'placa' =>  $entity->getPlaca(),
			'rodas' =>  $entity->getRodas(),
		);
	}

}
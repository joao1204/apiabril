<?php
declare(strict_types=1);

require "vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use TExAPITest\Repository\CarroRepository;

/**
 * @covers CarroRepository
 */
final class CarrosTest extends TestCase
{
    
    public function testGetCarById()
    {
        $CarroRepository = new CarroRepository();
        $this->assertInternalType('array', $CarroRepository->buscarPorId(1));
    }

    public function testCreateNewCar()
    {
        $CarroRepository = new CarroRepository();
        $this->assertInternalType('array', $CarroRepository->novo('{"modelo": "modelo1", "placa": "ABC-1234", "rodas": 4}'));
    }

    public function testCanDeleteByValidId()
    {
        $CarroRepository = new CarroRepository();
        $this->assertInternalType('bool', $CarroRepository->apagar(9));
    }

    public function testCanDeleteByInvalidId()
    {
        $CarroRepository = new CarroRepository();
        $this->assertInternalType('bool', $CarroRepository->apagar(999));
    }

    
}